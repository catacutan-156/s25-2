const http = require("http");

const host = 4000;

const { products } = require("./storage/products.js");

const { users } = require("./storage/users.js");

// register a new endpoint that will allow us to create a new product

let server = http.createServer((req, res) => {
	if (req.url === "/" && req.method === "GET") {
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Welcome to our B156 Online Store");
	} else if (req.url === "/catalog" && req.method === "GET") {
		res.writeHead(200, {"Content-Type": "application/json"});
		res.write(JSON.stringify(products));
		res.end();
	} else if (req.url === "/register" && req.method === "POST") { 
			let requestBody = "";
			req.on("data", (clientData) => {
				requestBody += clientData;
			});

			req.on("end", () => {
				let convertedData = JSON.parse(requestBody);
				console.log(convertedData);	
				let newUser = {
					"emailAddress": convertedData.email, 
					"userPassword": convertedData.password,
					"mobileNumber": convertedData.mobileNo,
					"gender": convertedData.gender,
					"birthday": convertedData.birthDate
				}

				users.push(newUser);
				res.writeHead(201, {"Content-Type": "application/json"});
				res.end("Successfully created new account!");
			});
	} else if (req.url === "/admin" && req.method === "GET") {
		res.writeHead(200, {"Content-Type": "application/json"});
		res.end(JSON.stringify(users));
	} else if (req.url == "/create" && req.method === "POST") {
		// declare a component that would describe the body section/input of the request.
		let reqBody = "";
		// identify what will be the anatomy of a product
			// (name, price, stock, color, category)
		// Create a template in Postman
		// Start creating the streams for us to be able to emit events needed to create this new data.
		req.on("data", (productInput) => {
			reqBody += productInput;
			console.log(reqBody);
		});

		// create a response "end" step -> this will only run after the request has been completely sent.
		req.on("end", () => {
			let usableData = JSON.parse(reqBody);
			console.log(usableData);

			let newItem = {
				"itemName": usableData.name, 
				"itemPrice": usableData.price,
				"stocks": usableData.stock,
				"color": usableData.color,
				"category": usableData.category
			}

			products.push(newItem);
			res.writeHead(201, {"Content-Type": "application/json"});
			res.write(" A new product has been created!");
			res.end();
		})
	} else {
		res.writeHead(404, {"Content-Type": "text/plain"});
		res.end("No page was Found");
	} 
});

server.listen(host);

console.log(`Listening on port: ${host}`);

// How to create a backup/copy of your Postman collections?